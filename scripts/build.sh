#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")"/../

  scripts/clean.sh

  from="gitlab.com"
  lastVer="0.0.1"
  org="softwa"
  repo="very-hungry-penguins"

  wget https://${from}/${org}/${repo}/repository/${lastVer}/archive.tar.gz

  newArchiveName=${repo}_${lastVer}.orig.tar.gz

  mv archive.tar.gz $newArchiveName
  tar xf $newArchiveName

  newName=${repo}-${lastVer}
  if [ ! -f $newName ]; then
      mv "$(ls | grep ${repo}-${lastVer}-)" $newName
  fi

  cd $newName
  cp -r ../debian/ debian/
  # preserve PATH to find opam installed libraries
  debuild --preserve-envvar PATH -us -uc
)
