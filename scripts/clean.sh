#!/usr/bin/env sh

set -eu

(
  cd "$(dirname "$0")"/../
  rm -rf *.tar.gz *.build *.dsc *.tar.xz *.buildinfo *.changes *.deb
  rm -rf $(ls | grep very-hungry-penguins-)
)
