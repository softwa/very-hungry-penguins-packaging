#!/usr/bin/env sh

set -eux

(
  cd "$(dirname "$0")"/../

  from="gitlab.com"
  lastVerNum="0.0.1"
  lastVer="v${lastVerNum}"
  org="softwa"
  repo="very-hungry-penguins"
  destDir="../${org}/${repo}/repository/${lastVer}"
  mkdir -p $destDir
  (
    cd $destDir
    wget https://${from}/${org}/${repo}/repository/${lastVer}/archive.tar.gz
  )
  uscan
)
