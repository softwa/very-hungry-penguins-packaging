#!/usr/bin/env sh

set -eu

apt -yqq update
apt -yqq install devscripts wget

apt -yqq install ocaml opam oasis shellcheck m4 libgtk2.0-dev libyojson-ocaml libyojson-ocaml-dev

opam init -a
# shellcheck disable=SC2046
eval $(opam config env)
opam update -y
opam install -y oasis alcotest yojson lablgtk
